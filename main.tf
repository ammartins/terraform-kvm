provider "libvirt" {
  uri = "qemu:///system"
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config.cfg")
}

module "main-node" {
  source = "./modules/main-nodes"
  providers = {
    libvirt = libvirt
  }

  network_config  = data.template_file.network_config.rendered
  user_data       = data.template_file.user_data.rendered
  node_name       = "main-node"
  nodes           = var.nodes
}

module "worker-nodes" {
  source = "./modules/worker-nodes"
  providers = {
    libvirt = libvirt
  }

  number_nodes  = var.number_nodes

  network_config  = data.template_file.network_config.rendered
  user_data       = data.template_file.user_data.rendered

  nodes           = var.nodes
  pool_path       = var.pool_path
}
