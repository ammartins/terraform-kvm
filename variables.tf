variable "number_nodes" {
  type = number
}

variable "nodes" {
  type = object({
    cpu     = number
    memory  = string
  })
}

variable "pool_path" {}