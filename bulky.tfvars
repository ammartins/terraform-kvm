number_nodes = 5

nodes = {
  cpu = 8
  memory = "10240"
}

pool_path = "/src/terraform-provider-libvirt-pool-node"