variable "node_name" {}

variable "user_data" {}
variable "network_config" {}

variable "nodes" {
  type = object({
    cpu     = number
    memory  = string
  })
}