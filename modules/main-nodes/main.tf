resource "libvirt_pool" "node-master" {
  name = "${var.node_name}-pool"
  type = "dir"
  path = "/src/terraform-provider-libvirt-pool-node-master"
}

resource "libvirt_volume" "ubuntu-2004" {
  name   = "${var.node_name}-volume"
  pool   = libvirt_pool.node-master.name
  source = "https://cloud-images.ubuntu.com/releases/24.04/release/ubuntu-24.04-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_cloudinit_disk" "init-master" {
  name           = "${var.node_name}-disk"
  user_data      = var.user_data
  network_config = var.network_config
  pool           = libvirt_pool.node-master.name
}

resource "libvirt_domain" "kube-master" {
  name   = "${var.node_name}"
  memory = var.nodes.memory
  vcpu   = var.nodes.cpu

  cloudinit = libvirt_cloudinit_disk.init-master.id

  network_interface {
    network_name  = "default"
    hostname      = "kubemaster"
    addresses     = ["192.168.122.10"]
    wait_for_lease = true
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.ubuntu-2004.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}