variable "number_nodes" {
  type = number
}

variable "nodes" {
  type = object({
    cpu     = number
    memory  = string
  })
}

variable "user_data" {}
variable "network_config" {}
variable "pool_path" {}