resource "libvirt_pool" "node-pool" {
  count = var.number_nodes

  name = "node-${count.index}"
  type = "dir"
  path = "${var.pool_path}-${count.index}"
}

resource "libvirt_volume" "node-volume" {
  count = var.number_nodes

  name   = "node-${count.index}"
  pool   = libvirt_pool.node-pool[count.index].name

  source = "https://cloud-images.ubuntu.com/releases/24.04/release/ubuntu-24.04-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_cloudinit_disk" "node-disks" {
  count = var.number_nodes

  name           = "init-node1"
  user_data      = var.user_data
  network_config = var.network_config
  pool           = libvirt_pool.node-pool[count.index].name
}

resource "libvirt_domain" "kube-node" {
  count = var.number_nodes

  name   = "kube-node${count.index}"
  memory = var.nodes.memory
  vcpu   = var.nodes.cpu

  cloudinit = libvirt_cloudinit_disk.node-disks[count.index].id

  network_interface {
    network_name = "default"
    hostname      = "kube-node-${count.index}"
    addresses     = ["192.168.122.2${count.index}"]
    wait_for_lease = true
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.node-volume[count.index].id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}